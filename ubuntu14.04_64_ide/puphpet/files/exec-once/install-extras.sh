# Add SMARF repo
#echo "deb http://sw.rpsmarf.ca/debs all/" >> /tmp/new_source
#cat /etc/apt/sources.list /tmp/new_source > /tmp/sources.list
#sudo mv /tmp/sources.list /etc/apt/sources.list

#Install Java 
# echo "*****Installing JAVA..."
# wget -q --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u60-b19/jdk-7u60-linux-x64.tar.gz
# tar -xf jdk-7u60-linux-x64.tar.gz 
# sudo mv jdk1.7.0_60 /opt

echo 'JAVA_HOME=/opt/jdk1.7.0_60\nexport PATH=$PATH:$JAVA_HOME/bin' >> /home/vagrant/.profile

#sudo ln -sf /opt/jdk1.7.0_60/bin/java /usr/bin/java

# Download and compile Python 3.4
#echo "*****Installing Python3.4"
#sudo apt-get -y install build-essential libsqlite3-dev sqlite3 bzip2 libbz2-dev libz-dev libssl-dev
#wget -q http://www.python.org/ftp/python/3.4.1/Python-3.4.1.tar.xz
#tar xJf ./Python-3.4.1.tar.xz
#cd ./Python-3.4.1
#./configure -q
#make -s && sudo make -s install

# Install pip and virtualenv
#sudo apt-get -y install python-pip
#sudo -H pip install virtualenv
#sudo -H virtualenv /opt/python3.4 -p python3.4


# Install django, tastypie, etc... for Python3
#echo "*****Installing python addons..."
# sudo bash -c "source /opt/python3.4/bin/activate && pip install django==1.6.5"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install django-tastypie"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install requests"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install paramiko"
#sudo apt-get -y install build-essential python-dev libmysqlclient-dev
#sudo apt-get -y install build-essential libmysqlclient-dev

# sudo bash -c "source /opt/python3.4/bin/activate && pip install cymysql"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install django-cymysql"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install uwsgi"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install pytz"
# sudo bash -c "source /opt/python3.4/bin/activate && pip install django-cors-headers"



# cd /var/www/rpsmarf/theapp

#sudo su www-data

#echo "*****Installing emacs ..."
#sudo apt-get -y -q install emacs23
#sudo apt-get -y -q install tree

# echo "***** Creating user rpsmarf"
# sudo useradd -m -g rpsmarf rpsmarf -s /bin/bash -G sudo
# echo rpsmarf:smarF.2014 | chpasswd
# echo "rpsmarf ALL=NOPASSWD:ALL" >> rpsmarf
# chmod 440 rpsmarf
# sudo mv rpsmarf /etc/sudoers.d


# sudo cp ~rpsmarf/.bashrc bashrc_with_extras
# echo "export JAVA_HOME=/opt/jdk1.7.0_60" >> bashrc_with_extras
# echo "export EDITOR=vi" >> bashrc_with_extras
# echo "alias fixb="$EDITOR ~/.bashrc; source ~/.bashrc"" >> bashrc_with_extras

# echo "export PATH=$PATH:/opt/jdk1.7.0_60/bin" >> bashrc_with_extras
# echo 'alias sm_activate_3.4="source /opt/python3.4/bin/activate"' >> bashrc_with_extras
# echo 'alias cdg="cd ~/git"' >> bashrc_with_extras
# echo sm_activate_3.4 >> bashrc_with_extras

# sudo mv bashrc_with_extras ~rpsmarf/.bashrc
# sudo chown rpsmarf  ~rpsmarf/.bashrc

# echo '(setq make-backup-files nil)' >> .emacs
# sudo cp .emacs ~rpsmarf/.emacs
# sudo cp -r /root/.ssh ~rpsmarf/.ssh

# sudo chown -R rpsmarf  ~rpsmarf/.

#echo "Installing ICE..."
#cd /tmp
#sudo apt-get -y purge ice
#sudo apt-get -y purge libicessl35

#wget http://www.zeroc.com/download/RPM-GPG-KEY-zeroc-release
#sudo apt-key add RPM-GPG-KEY-zeroc-release
#cd /etc/apt/sources.list.d
#sudo wget http://www.zeroc.com/download/Ice/3.5/ubuntu/ice3.5-trusty.list
#sudo apt-get update
#sudo apt-get -y install ice icebox/trusty-zeroc icegrid/trusty-zeroc python3-zeroc-ice
# Copy the ICE files from the system folder to the Python virtual env
#sudo cp -r /usr/lib/python3/dist-packages/Ice* /opt/python3.4/lib/python3.4/site-packages/
# We need to rename this .so to make it work
#sudo cp /opt/python3.4/lib/python3.4/site-packages/IcePy.cpython-34m-x86_64-linux-gnu.so /opt/python3.4/lib/python3.4/site-packages/IcePy.so

#echo "*****Installing ICE slice tools..."
#sudo apt-get -y install ice35-translators


echo "=========================================================="
echo "   IDE IMAGE ITEMS NEXT"
echo "=========================================================="

#sudo bash -c "source /opt/python3.4/bin/activate && pip install doit"
#sudo bash -c "source /opt/python3.4/bin/activate && pip install flake8"


echo "*****Installing Eclipse..."
wget -q http://mirror.csclub.uwaterloo.ca/eclipse/eclipse/downloads/drops4/R-4.4-201406061215/eclipse-SDK-4.4-linux-gtk-x86_64.tar.gz
tar -xzf eclipse-SDK-4.4-linux-gtk-x86_64.tar.gz
mv eclipse /opt/
sudo chown -R root:root /opt/eclipse
sudo chmod -R +r /opt/eclipse
sudo touch /usr/bin/eclipse
sudo chmod 755 /usr/bin/eclipse

echo '#!/bin/sh' >> eclipse
echo 'export ECLIPSE_HOME="/opt/eclipse"' >> eclipse
echo 'export PATH=$PATH:/opt/jdk1.7.0_60/bin' >> eclipse 
echo '$ECLIPSE_HOME/eclipse $*' >> eclipse
sudo mv eclipse /usr/bin
sudo chmod a+x /usr/bin/eclipse 

echo "[Desktop Entry]" >> eclipse.desktop
echo "Encoding=UTF-8" >> eclipse.desktop
echo "Name=Eclipse" >> eclipse.desktop
echo "Comment=Eclipse IDE" >> eclipse.desktop
echo "Exec=/usr/bin/eclipse" >> eclipse.desktop
echo "Icon=/opt/eclipse/icon.xpm" >> eclipse.desktop
echo "Terminal=false" >> eclipse.desktop
echo "Type=Application" >> eclipse.desktop
echo "Categories=GNOME;Application;Development;" >> eclipse.desktop
echo "StartupNotify=true" >> eclipse.desktop
sudo mv eclipse.desktop /usr/share/applications/

#echo "*****Install tools for DEB packaging..."
#sudo apt-get install build-essential autoconf automake autotools-dev dh-make debhelper devscripts fakeroot xutils lintian pbuilder

#echo "*****Installing Ubuntu desktop..."
#sudo apt-get -y -q update
#sudo apt-get -y -q install ubuntu-gnome-desktop

sudo su rpsmarf
mkdir /home/rpsmarf/git
git clone git@bitbucket.org:rpsmarf/python_repo.git
git clone git@bitbucket.org:rpsmarf/vmconfig.git
echo "export SMARF_GITDIR=/home/rpsmarf/git/python_repo" >> ~/.bashrc
echo "source $SMARF_GITDIR/tools/bash_script" >> ~/.bashrc

