# README File #

### What is this repository for? ###

This repository contains configurations for development VMs.

### How do I get set up? ###

* Install vagrant
* Checkout this repo
* Go to the appropriate folder (e.g. ubuntu12.04_64 and type "vagrant up"


### Who do I talk to? ###

* andrewmcgregor@sce.carleton.ca
* melendez@sce.carleton.ca
* anshuman@sce.carleton.ca
